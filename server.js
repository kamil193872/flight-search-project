const express = require('express'),
	bodyParser = require('body-parser'),
	got = require('got'),
	app = express();

const URL_AIRLINES 	= "http://node.locomote.com/code-task/airlines",
	URL_AIRPORTS	= "http://node.locomote.com/code-task/airports?q=",
	URL_SEARCH		= "http://node.locomote.com/code-task/flight_search/";

	
//config
app.use(express.static('public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

//helper
function callApi(URL){
	return got(URL).then(response=>{
		return JSON.parse(response.body);
	}).catch(err=>{
		console.warn(err);
	});
}

app.get('/', function (req, res) {
  res.file("public/index.html");
})

app.get('/airlines', function (req, res) {
	callApi(URL_AIRLINES).then(response=>{
		res.json(response);
	});
})

app.get('/airports', function (req, res) {
	callApi(URL_AIRPORTS + req.query.q).then(response=>{
		res.json(response);
	});
})

app.post('/search', function (req, res) {
	callApi(URL_AIRLINES).then(response=>{
		if( ! (response instanceof Array)) throw Error("Fetch data fail");
		
		let promiseTab 	= [],
			params		= req.body;
		
		response.forEach(airline=>{
			let url = URL_SEARCH + airline.code + "?date=" + params.date + "&from=" + params.from + "&to=" + params.dest;
			promiseTab.push(callApi(url));
		});
		
		Promise.all(promiseTab).then((response,xx)=>{
			res.json(response);
		});
		
		
		
		
	}).catch(err=>{
		console.log(err);
		res.json({status: false});
	});
})


app.listen(3000, function () {
  console.log('Server is up and listening...')
})