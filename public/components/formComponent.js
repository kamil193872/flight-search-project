window.formComponent = function(_formName, _selector, onSubmit){
	var source   	= $("#search-form-template").html(),
		template 	= Handlebars.compile(source),
		formName 	= _formName,
		selector	= _selector,
		html 	 	= null,
		datePicker	= null,
		inputFrom	= "",
		inputDest	= "";
		
		
	function initElements(html){
		inputFrom 	= $(html).find("#fn")[0];
		inputDest 	= $(html).find("#dest")[0];
		datePicker 	= $(html).find('#datetimepicker12').datetimepicker({
				inline: true,
				format: 'YYYY MM DD',
				sideBySide: true
			});
		let ajaxParams = {
			tags: true,
			multiple: false,
			tokenSeparators: [',', ' '],
			minimumInputLength: 2,
			minimumResultsForSearch: 10,
			ajax: {
				url: "/airports",
				dataType: "json",
				type: "GET",
				data: function (params) {

					var queryParameters = {
						q: params.term
					}
					return queryParameters;
				},
				processResults: function (data) {
					return {
						results: $.map(data, function (item) {
							return {
								text: item.cityName +" - " + item.airportName,
								id: item.airportCode
							}
						})
					};
				}
			}
		}
		
		$(html).find("#fn").select2(ajaxParams);
		$(html).find("#dest").select2(ajaxParams);
		$(html).find('button').click(()=>{
			onSubmit({
				from: 	$(inputFrom).val(),
				dest:	$(inputDest).val(),
				date:	$(html).find('#datetimepicker12').data('DateTimePicker').date().format('YYYY-MM-DD')
			});
		});
	}
		
	this.render = function(){
		var context = {formName},
			html    = $(template(context))[0];
			
		initElements(html);
			
		$(selector).append(html);
		return this;
	};
	
	return this.render();
}