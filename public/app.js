var	URL_AIRLINES 	= "/airlines",
	URL_SEARCH		= "/search";

function fetchData(){
	return new Promise((resolve, reject)=>{
		fetch(URL_AIRLINES)
		.then(rowResponse=>rowResponse.json())
		.then(function(response){
			resolve(response);
		})
		.catch(err=>{
			console.log("Fetch data fail :(");
			reject();
		});
	
	
	});
	
}
function hideLoader(){
	return new Promise((resolve, reject)=>{
		$('.loader').fadeOut(null,function(){
			$('div.nav, .view-holder, footer').fadeIn();	
			resolve();
		});
	});
}
function showLoader(){
	return new Promise((resolve, reject)=>{
		
		$('div.nav, .view-holder, footer').fadeOut(null,function(){
			$('.loader').fadeIn();	
			resolve();
		});
		
		
	});
}

function showModal(){
	$("#addModal").modal("show");
}

function searchFlight(data){
	console.log(data);
	var headers = new Headers();
	headers.append("Content-Type", "application/json");
	
	showLoader();
	
	fetch(URL_SEARCH, {method: "POST", headers:headers, body:JSON.stringify(data)})
		.then(rawResponse=> rawResponse.json())
		.then(response=>{
			var htmlObj = $('<ol>');
			
			if(response instanceof Array){
				response.forEach(airLinesTab=>{
					if(!(airLinesTab instanceof Array))return;
					var obj = {},
						ulHtml = $('<ul>'+airLinesTab[0].airline.name+'</ul>');
					
					airLinesTab.forEach(item=>{
						if(!item)return;
						
						if(!obj[item.airline.name]) obj[item.airline.name] = [];
						var flight = {distance: item.distance, 
									duration: item.durationMin, 
									start: item.start.dateTime}
						
						obj[item.airline.name].push(flight);
						var time = moment(item.start.dateTime).format('DD-MM-YYYY HH:MM'); 
						var li = $('<li>distance: '+item.distance+", duration: "+item.durationMin+ ", start: "+time+'</li>');
						ulHtml.append(li);
					});

					htmlObj.append(ulHtml);
				});
			}
			
			hideLoader().then(()=>{
				
				$('#search-reasult').html(htmlObj);
				showModal();
			});
		}).catch(err=>{
			console.warn(err);
		});
	
}

function createSearchFields(){
	new formComponent("Flight", '#insert-container', searchFlight);
	new formComponent("Returning Flight", '#insert-container', searchFlight);
}

$(document).ready(function(){
	
	hideLoader().then(()=>{
		createSearchFields();
	});

});